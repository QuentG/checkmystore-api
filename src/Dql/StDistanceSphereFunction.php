<?php

namespace App\Dql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Query\SqlWalker;

/**
 * StDistanceSphereFunction ::= "ST_Distance_Sphere" "(" "Identifier" "(" ArithmeticPrimary "," ArithmeticPrimary ")" ", " "Identifier" "(" ArithmeticPrimary "," ArithmeticPrimary ")" ")"
 */
class StDistanceSphereFunction extends FunctionNode
{
	public $lat1 = 0;
	public $lat2 = 0;
	public $lng1 = 0;
	public $lng2 = 0;

	/**
	 * @param Parser $parser
	 *
	 * @return void
	 * @throws QueryException
	 */
	public function parse(Parser $parser)
	{
		// ST_Distance_Sphere '('
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);

		// Point
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->lat1 = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_COMMA);
		$this->lng1 = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);

		// ","
		$parser->match(Lexer::T_COMMA);

		// Point
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->lat2 = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_COMMA);
		$this->lng2 = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);

		// ST_Distance_Sphere ')'
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);

	}

	/**
	 * @param SqlWalker $sqlWalker
	 *
	 * @return string
	 */
	public function getSql(SqlWalker $sqlWalker)
	{
		return 'ST_Distance_Sphere('.
			'point('.$this->lat1->dispatch($sqlWalker) . ', ' . $this->lng1->dispatch($sqlWalker) . '), ' .
			'point('.$this->lat2->dispatch($sqlWalker) . ', ' . $this->lng2->dispatch($sqlWalker) . ')' .
			')';
	}
}
