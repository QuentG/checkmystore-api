<?php

namespace App\Dql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Query\SqlWalker;

class RoundFunction extends FunctionNode
{
	private $arithmeticExpression;
	private $arround;

	/**
	 * @param Parser $parser
	 *
	 * @return void
	 * @throws QueryException
	 */
	public function parse(Parser $parser)
	{
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);

		$this->arithmeticExpression = $parser->ArithmeticExpression();

		$parser->match(Lexer::T_COMMA);

		$this->arround = $parser->ArithmeticExpression();

		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	/**
	 * @param SqlWalker $sqlWalker
	 *
	 * @return string
	 */
	public function getSql(SqlWalker $sqlWalker)
	{
		return 'ROUND(' . $this->arithmeticExpression->dispatch($sqlWalker) . ', '
			. $this->arround->dispatch($sqlWalker) .')';
	}

}
