<?php

namespace App\Manager;

use App\Entity\Market;
use App\Entity\User;
use App\Enum\EntityFieldEnum;
use Doctrine\ORM\EntityManagerInterface;

class MarketManager
{
	/** @var EntityManagerInterface */
	private $entityManager;
	/**
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * @param array $infos
	 * @param User $user
	 */
	public function createMarket(array $infos, User $user)
	{
		$market = new Market();
		$market->setName($infos[EntityFieldEnum::NAME_FIELD])
			->setAddress($infos[EntityFieldEnum::ADDRESS_FIELD])
			->setCity($infos[EntityFieldEnum::CITY_FIELD])
			->setLatitude($infos[EntityFieldEnum::LAT_FIELD])
			->setLongitude($infos[EntityFieldEnum::LNG_FIELD])
			->setZipCode($infos[EntityFieldEnum::ZIPCODE_FIELD])
			->setUser($user);

		$this->entityManager->persist($market);
		$this->entityManager->flush();
	}

	/**
	 * @param Market $market
	 * @param array $infos
	 */
	public function editMarket(Market $market, array $infos)
	{
		if (array_key_exists($infos[EntityFieldEnum::TIME_FIELD], $infos)) {
			$market->setWaitingTime($infos[EntityFieldEnum::TIME_FIELD]);
		}

		$this->entityManager->flush();
	}

}