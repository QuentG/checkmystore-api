<?php

namespace App\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class ApiTokenManager
{
	private const TOKEN_VALIDATION_DURATION = 'P2D'; // 2 days

	/** @var EntityManagerInterface */
	private $entityManager;
	/** @var UserRepository|ObjectRepository */
	private $apiTokenRepository;

	/**
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
		$this->apiTokenRepository = $entityManager->getRepository(User::class);
	}

	/**
	 * @param User $user
	 *
	 * @return User
	 */
	public function create(User $user)
	{
		$tokens = $this->generateTokens();

		$user->setAccessToken($tokens['accessToken'])
			->setExpirationDate($tokens['expirationDate'])
			->setRefreshToken($tokens['refreshToken']);

		// Insert new ApiToken in DB
		$this->entityManager->flush();

		return $user;
	}

	/**
	 * @param User $apiToken
	 *
	 * @return User
	 */
	public function refreshToken(User $apiToken)
	{
		$tokens = $this->generateTokens();

        $apiToken->setAccessToken($tokens['accessToken']);
        $apiToken->setRefreshToken($tokens['refreshToken']);
        $apiToken->setExpirationDate($tokens['expirationDate']);

		$this->entityManager->persist($apiToken);
		$this->entityManager->flush();

		return $apiToken;
	}

	/**
	 * @return array
	 */
	private function generateTokens()
	{
		$tokens = [];

		do {
			$accessToken = sha1(random_bytes(10));
		} while ($this->accessTokenAlreadyExists($accessToken));

		$tokens['accessToken'] = $accessToken;

		do {
			$refreshToken = sha1(random_bytes(10));
			$refreshToken = '__'.substr($refreshToken, 0, -2); // refreshToken starts by '__' to more easily identify it
		} while ($this->refreshTokenAlreadyExists($refreshToken));

		$tokens['refreshToken'] = $refreshToken;
		$tokens['expirationDate'] = (new \DateTime('now'))->add(new \DateInterval(self::TOKEN_VALIDATION_DURATION)); // ApiToken is valid 2 days

		return $tokens;
	}

	/**
	 * @param string $accessToken
	 *
	 * @return bool
	 */
	private function accessTokenAlreadyExists(string $accessToken)
	{
		$apiToken = $this->apiTokenRepository->findOneBy([
			'accessToken' => $accessToken
		]);

		return !is_null($apiToken);
	}

	/**
	 * @param string $refreshToken
	 *
	 * @return bool
	 */
	private function refreshTokenAlreadyExists(string $refreshToken)
	{
		$refreshToken = $this->apiTokenRepository->findOneBy([
			'refreshToken' => $refreshToken
		]);

		return !is_null($refreshToken);
	}

}