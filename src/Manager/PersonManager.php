<?php

namespace App\Manager;

use App\Entity\Market;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonManager
{
	/** @var EntityManagerInterface */
	private $entityManager;

	/**
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * @param Market $market
	 */
	public function create(Market $market)
	{
		$person = new Person();
		$person->setCreatedAt((new \DateTime())->add(new \DateInterval('PT2H')))
			->addMarket($market);

		$this->entityManager->persist($person);
		$this->entityManager->flush();
	}

	/**
	 * @param Market $market
	 * @param float $lat
	 * @param float $lng
	 * @param int $persons
	 *
	 * @return array
	 */
	public function getPersons(Market $market, $lat, $lng, int $persons)
	{
		$distance = $this->entityManager->getRepository(Market::class)->getDistance(
			$market->getId(),
			floatval($lat),
			floatval($lng)
		);

		$waitingTime = $market->getWaitingTime() === 0 ? 10 : $market->getWaitingTime();

		return [
			'id' => $market->getId(),
			'name' => $market->getName(),
			'distance' => $distance[1],
			'persons' => $persons,
			'waitingTime' => $waitingTime * $persons
		];
	}

}