<?php

namespace App\Manager;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Enum\EntityFieldEnum;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager
{
	/** @var EntityManagerInterface */
	private $entityManager;
	/** @var UserPasswordEncoderInterface */
	private $passwordEncoder;
	/** @var ApiTokenManager */
	private $apiTokenManager;

	/**
	 * @param EntityManagerInterface $entityManager
	 * @param UserPasswordEncoderInterface $passwordEncoder
	 */
	public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, ApiTokenManager $apiTokenManager)
	{
		$this->entityManager = $entityManager;
		$this->passwordEncoder = $passwordEncoder;
		$this->apiTokenManager = $apiTokenManager;
	}

	/**
	 * @param string $email
	 * @param string $password
	 *
	 * @return User
	 */
	public function create(string $email, string $password)
	{
		$user = new User();
		$user->setEmail($email)
			->setPassword($this->passwordEncoder->encodePassword($user, $password));

		$this->apiTokenManager->create($user);

		$this->entityManager->persist($user);
		$this->entityManager->flush();

		return $user;
	}

	/**
	 * @param User $user
	 * @param $jsonData
	 */
	public function editUser(User $user, $jsonData)
	{
		if (array_key_exists(EntityFieldEnum::EMAIL_FIELD, $jsonData)) {
			$user->setEmail($jsonData[EntityFieldEnum::EMAIL_FIELD]);
		}

		if (array_key_exists(EntityFieldEnum::PASSWORD_FIELD, $jsonData)) {
			$user->setPassword($this->passwordEncoder->encodePassword($user, $jsonData[EntityFieldEnum::PASSWORD_FIELD]));
		}

		$this->entityManager->flush();
	}

}