<?php

namespace App\Controller;

use App\Entity\Market;
use App\Entity\Person;
use App\Entity\User;
use App\Enum\EntityFieldEnum;
use App\Helper\MarketHelper;
use App\Manager\MarketManager;
use App\Manager\PersonManager;
use App\Repository\MarketRepository;
use App\Repository\PersonRepository;
use App\Utils\Utils;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MarketController extends AbstractController
{
	/** @var EntityManagerInterface */
	private $entityManager;
	/** @var Utils */
	private $utils;
	/** @var MarketManager */
	private $marketManager;
	/** @var MarketHelper */
	private $marketHelper;
	/** @var MarketRepository|ObjectRepository */
	private $marketRepository;
	/** @var PersonManager */
	private $personManager;
	/** @var PersonRepository|ObjectRepository */
	private $personRepository;

	/**
	 * @param EntityManagerInterface $entityManager
	 * @param Utils $utils
	 * @param MarketManager $marketManager
	 * @param MarketHelper $marketHelper
	 * @param PersonManager $personManager
	 */
	public function __construct
	(
		EntityManagerInterface $entityManager,
		Utils $utils,
		MarketManager $marketManager,
		MarketHelper $marketHelper,
		PersonManager $personManager
	)
	{
		$this->entityManager = $entityManager;
		$this->utils = $utils;
		$this->marketManager = $marketManager;
		$this->marketHelper = $marketHelper;
		$this->marketRepository = $entityManager->getRepository(Market::class);
		$this->personManager = $personManager;
		$this->personRepository = $entityManager->getRepository(Person::class);
	}

	/**
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function createMarket(Request $request)
	{
		$jsonData = \json_decode($request->getContent(), true);
		if (null === $jsonData) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'incorrect_json');
		}

		/** @var User $user */
		$user = $this->getUser();

		$this->marketManager->createMarket($jsonData, $user);

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'market_created');
	}

	/**
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function findMarkets(Request $request)
	{
		$jsonData = $request->query->all(); // Get all params
		if (!array_key_exists(EntityFieldEnum::LAT_FIELD, $jsonData) || !array_key_exists(EntityFieldEnum::LNG_FIELD, $jsonData) || !array_key_exists(EntityFieldEnum::KM_FIELD, $jsonData)){
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'missing_fields');
		}

		$markets = $this->marketRepository->calcDistance(
			floatval($jsonData[EntityFieldEnum::LAT_FIELD]),
			floatval($jsonData[EntityFieldEnum::LNG_FIELD]),
			floatval($jsonData[EntityFieldEnum::KM_FIELD])
		);

		if (null === $markets) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'empty_markets');
		}

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', '', $this->marketHelper->getMarketsInfos($markets));
	}

	/**
	 * @param int $id
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function getPersonsByTimestamp(int $id, Request $request)
	{
		$jsonData = $request->query->all(); // Get all param

		$market = $this->marketRepository->find($id);
		if (null === $market) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'market_not_found');
		}

		$persons = $this->personRepository->getPersonsByTimestamp(
			$id,
			$jsonData[EntityFieldEnum::START_FIELD]
		);

		$previousPersons = $this->personRepository->getPreviousPersons($id, $jsonData[EntityFieldEnum::START_FIELD]);
		$total = intval($persons[0][1]) + intval($previousPersons[0][1]);

		$result = $this->personManager->getPersons($market, $jsonData[EntityFieldEnum::LAT_FIELD], $jsonData[EntityFieldEnum::LNG_FIELD], $total);

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', '', $result);
	}

	/**
	 * @param integer $id
	 *
	 * @return JsonResponse
	 */
	public function createPerson(int $id)
	{
		$market = $this->marketRepository->find($id);
		if (null === $market) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'market_not_found');
		}

		$this->personManager->create($market);

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'person_created');
	}

	/**
	 * @param int $id
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function changeSettings(int $id, Request $request)
	{
		$jsonData = \json_decode($request->getContent(), true);
		if (null === $jsonData) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'incorrect_json');
		}

		$market = $this->marketRepository->find($id);
		if (null === $market) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'market_not_found');
		}

		$this->marketManager->editMarket($market, $jsonData);

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'market_edited');
	}

	/**
	 * @return JsonResponse
	 */
	public function getMyInfos()
	{
		/** @var User $user */
		$user = $this->getUser();
		$infos = [];

		$markets = [];
		if (!$user->getMarkets()->isEmpty()) {
			foreach ($user->getMarkets() as $market) {
				$markets[] = [
					'name' => $market->getName(),
					'address' => $market->getAddress(),
					'city' => $market->getCity(),
					'zip_code' => $market->getZipCode()
				];
			}
		}

		$infos[] = [
			'email' => $user->getEmail(),
			'markets' => $markets
		];

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', '', $infos);
	}

}