<?php

namespace App\Controller;

use App\Entity\User;
use App\Enum\EntityFieldEnum;
use App\Helper\UserHelper;
use App\Manager\ApiTokenManager;
use App\Manager\UserManager;
use App\Utils\Utils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends AbstractController
{
	/** @var EntityManagerInterface */
	private $entityManager;
	/** @var ApiTokenManager */
	private $apiTokenManager;
	/** @var Utils */
	private $utils;
	/** @var UserManager */
	private $userManager;
	/** @var UserHelper */
	private $userHelper;

	/**
	 * @param EntityManagerInterface $entityManager
	 * @param ApiTokenManager $apiTokenManager
	 * @param Utils $utils
	 * @param UserManager $userManager
	 * @param UserHelper $userHelper
	 */
	public function __construct
	(
		EntityManagerInterface $entityManager,
		ApiTokenManager $apiTokenManager,
		Utils $utils,
		UserManager $userManager,
		UserHelper $userHelper
	)
	{
		$this->entityManager = $entityManager;
		$this->apiTokenManager = $apiTokenManager;
		$this->utils = $utils;
		$this->userManager = $userManager;
		$this->userHelper = $userHelper;
	}

	/**
	 * Check in Security/ApiAuthenticator.php to check the login process
	 */
	public function login()
	{
		/** @var User $user */
		$user = $this->getUser();

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'logged_successfully', [
			'accessToken' => $user->getAccessToken(),
			'refreshToken' => $user->getRefreshToken(),
		]);
	}

	public function logout() {}

	/**
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function register(Request $request)
	{
		$jsonData = \json_decode($request->getContent(), true);
		if (null === $jsonData) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'incorrect_json');
		}

		if (!array_key_exists(EntityFieldEnum::EMAIL_FIELD, $jsonData) || !array_key_exists(EntityFieldEnum::PASSWORD_FIELD, $jsonData)) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'missing_fields');
		}

		$email = $jsonData[EntityFieldEnum::EMAIL_FIELD];
		$password = $jsonData[EntityFieldEnum::PASSWORD_FIELD];

		if (!$this->utils->validate($email)) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'incorrect_email_format');
		}

		if ($this->userHelper->userAlreadyExist($email)) {
			return $this->utils->formatResponseApi(Response::HTTP_OK, 'error', 'email_already_used');
		}

		$user = $this->userManager->create($email, $password);

		return $this->utils->formatResponseApi(Response::HTTP_OK, 'success', 'account_created', [
			'accessToken' => $user->getAccessToken(),
			'refreshToken' => $user->getRefreshToken(),
		]);
	}
}