<?php

namespace App\Repository;

use App\Entity\Market;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Market|null find($id, $lockMode = null, $lockVersion = null)
 * @method Market|null findOneBy(array $criteria, array $orderBy = null)
 * @method Market[]    findAll()
 * @method Market[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Market::class);
    }

	/**
	 * Retourne les informations des magasins les plus proches en fonction de la position du client
	 * et de son nombre de kilomètres.
	 *
	 * @param float $lat
	 * @param float $lng
	 * @param float $km
	 *
	 * @return mixed
	 */
    public function calcDistance(float $lat, float $lng, float $km)
	{
		$markets = $this->createQueryBuilder('m')
			->where('ROUND(ST_Distance_Sphere(point(:lat, :lng), point(m.latitude, m.longitude)) * 0.001, 2) <= :km')
			->setParameters([
				'lat' => $lat,
				'lng' => $lng,
				'km' => $km
			])
			->getQuery()
			->getResult()
		;

		return empty($markets) ? null : $markets;
	}

	/**
	 * @param int $id
	 * @param float $lat
	 * @param float $lng
	 *
	 * @return mixed [KM]
	 *
	 * @throws NonUniqueResultException
	 */
	public function getDistance(int $id, float $lat, float $lng)
	{
		return $this->createQueryBuilder('m')
			->select('ROUND(ST_Distance_Sphere(point(:lat, :lng), point(m.latitude, m.longitude)) * 0.001, 2)')
			->where('m.id = :id')
			->setParameters([
				'id' => $id,
				'lng' => $lng,
				'lat' => $lat
			])
			->getQuery()
			->getOneOrNullResult()
		;
	}
}
