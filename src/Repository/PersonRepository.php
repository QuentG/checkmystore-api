<?php

namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

	/**
	 * @param int $id
	 * @param $start
	 *
	 * @return mixed
	 */
    public function getPersonsByTimestamp(int $id, $start)
	{
		return $this->createQueryBuilder('p')
			->select('COUNT(p.id)')
			->innerJoin('p.markets', 'm')
			->where('m.id = :id')
			->andWhere('p.createdAt BETWEEN :start AND :end')
			->setParameters([
				'id' => $id,
				':start' => (new \DateTime())->setTimestamp($start),
				':end' => (new \DateTime())->setTimestamp($start)->add(new \DateInterval('PT15M'))
			])
			->getQuery()
			->getResult()
		;
	}

	/**
	 * @param int $id
	 * @param $start
	 *
	 * @return mixed
	 */
	public function getPreviousPersons(int $id, $start)
	{
		return $this->createQueryBuilder('p')
			->select('COUNT(p.id)')
			->innerJoin('p.markets', 'm')
			->where('m.id = :id')
			->andWhere('p.createdAt BETWEEN :start AND :end')
			->setParameters([
				'id' => $id,
				':start' => (new \DateTime())->setTimestamp($start)->sub(new \DateInterval('PT15M')),
				':end' => (new \DateTime())->setTimestamp($start)
			])
			->getQuery()
			->getResult()
			;
	}
}
