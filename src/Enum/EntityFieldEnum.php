<?php

namespace App\Enum;

abstract class EntityFieldEnum
{
	// User
	const EMAIL_FIELD = 'email';
	const PASSWORD_FIELD = 'password';

	// ApiToken
	const REFRESH_TOKEN_FIELD = 'refreshToken';

	// Market
	const NAME_FIELD = 'name';
	const ADDRESS_FIELD = 'address';
	const CITY_FIELD = 'city';
	const ZIPCODE_FIELD = 'zip_code';
	const LAT_FIELD = 'lat';
	const LNG_FIELD = 'lng';
	const KM_FIELD = 'km';
	const START_FIELD = 'start';
	const END_FIELD = 'end';
	const TIME_FIELD = 'time';

	// PushNotificationToken
	const TOKEN_FIELD = 'token';
	const PREVIOUS_TOKEN_FIELD = 'previous_token';
}