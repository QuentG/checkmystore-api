<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Market;
use App\Manager\ApiTokenManager;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
	private $encoder;
	private $apiTokenManager;

	public function __construct(UserPasswordEncoderInterface $encoder, ApiTokenManager $apiTokenManager)
	{
		$this->encoder = $encoder;
		$this->apiTokenManager = $apiTokenManager;
	}

	public function load(ObjectManager $manager)
    {
    	$user = new User();
    	$user->setEmail('test@test.fr')
			->setPassword($this->encoder->encodePassword($user, 'test'));

    	$manager->persist($user);

		$this->apiTokenManager->create($user);

		$market = new Market();
		$market->setName('ALDI')
			->setAddress('376 Avenue Thiers')
			->setZipCode(33100)
			->setCity('Bordeaux')
			->setLatitude(44.853048)
			->setLongitude(-0.539627)
			->setUser($user);

		$manager->persist($market);

		$market2 = new Market();
		$market2->setName('YNOV')
			->setAddress('89 Quai des Chartrons')
			->setZipCode(33000)
			->setCity('Bordeaux')
			->setLatitude(44.8541898)
			->setLongitude(-0.5684943)
			->setUser($user);

		$manager->persist($market2);

		$market3 = new Market();
		$market3->setName('Monoprix')
			->setAddress('10 rue Lucien Faure')
			->setZipCode(33000)
			->setCity('Bordeaux')
			->setLatitude(44.8609179)
			->setLongitude(-0.5551283)
			->setUser($user);

		$manager->persist($market3);

		$market4 = new Market();
		$market4->setName('Carrefour Express')
			->setAddress('59 cours de Verdun')
			->setZipCode(33000)
			->setCity('Bordeaux')
			->setLatitude(44.8514904)
			->setLongitude(-0.5749146)
			->setUser($user);

		$manager->persist($market4);
	
		$manager->flush();
    }
}
