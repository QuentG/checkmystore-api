<?php

namespace App\Helper;

use App\Entity\Market;
use App\Repository\MarketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class MarketHelper
{
	/** @var MarketRepository|ObjectRepository */
	private $manager;

	/**
	 * @param EntityManagerInterface $manager
	 */
	public function __construct(EntityManagerInterface $manager)
	{
		$this->manager = $manager->getRepository(Market::class);
	}

	/**
	 * @param array $markets
	 *
	 * @return array
	 */
	public function getMarketsInfos(array $markets)
	{
		$infos = [];

		/** @var Market $market */
		foreach ($markets as $market) {
			$infos[] = [
				'id' => $market->getId(),
				'name' => $market->getName(),
				'address' => $market->getAddress(),
				'city' => $market->getCity(),
				'zip_code' => $market->getZipCode()
			];
		}

		return $infos;
	}
}