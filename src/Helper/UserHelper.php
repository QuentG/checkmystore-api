<?php

namespace App\Helper;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class UserHelper
{
    /** @var UserRepository|ObjectRepository */
    private $manager;

    /**
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager->getRepository(User::class);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function userAlreadyExist(string $email)
    {
        $user = $this->manager->findOneBy([
            'email' => $email
        ]);

        return !is_null($user);
    }

}